# Execute Empirical Studies

## Prerequisites

To replicate the results of our study, you'll need to have the following software installed on your computer:

- Docker
- Yarn
- Nodejs

## Replication Material for Synchronization Issues Empirical Study

### Setting Up Your Environment

#### Step 1: Directory and Repository Setup

1. Create five directories on your computer, and name them `0, 1, 2, 3, 4`.
2. Clone the following repositories into each directory:
    - [execute-study](https://gitlab.com/lsi-ufcg/cytestion/sync-study/execute-study)
    - [sylius-showcase](https://gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-showcase)
    - [sylius-test-suite](https://gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-test-suite)

For each directory:

- **Directory 0**: Use the `main` branch in the `sylius-showcase` repository.
- **Directory 1**: Use the `500ms` branch in the `sylius-showcase` repository.
- **Directory 2**: Use the `1000ms` branch in the `sylius-showcase` repository.
- **Directory 3**: Use the `1500ms` branch in the `sylius-showcase` repository.
- **Directory 4**: Use the `2000ms` branch in the `sylius-showcase` repository.

Make sure you've successfully checked out the correct branches and prepared the instances for the next steps.

#### Step 2: Sylius Showcase Configuration

Inside the `sylius-showcase` repository, you'll find instructions on building, running, and inserting sample data into the respective instance. Please follow these steps.

#### Step 3: Setting Up Permissions and Script Paths

1. You will need to grant your user account permission to execute scripts with `sudo` without requiring a password. Follow this [guide](https://www.cyberciti.biz/faq/linux-unix-running-sudo-command-without-a-password/) to set this up.
2. We will use this [restart](https://gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-showcase/restart.sh) script with `sudo` to reset the database of each container automatically.
3.  Ensure to adjust the path contained in `restart.sh` to match your current working directory. For example, replace it with `/<your workspace>/0/sylius-showcase/media`.

#### Step 4: Configuration for Delays Directories

We will set up the `execute-study` and `sylius-test-suite` repositories to work with Sylius instances running on ports different from the default (9990).

For directories that involve delays (i.e., `1, 2, 3, 4`), you'll need to make a few adjustments:

1. In the [index.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/execute-study/index.js) file for each directory, modify line 4 to point to the correct path. For instance, for directory `1`, it should be `/<your workspace>/1/sylius-test-suite`, and so on.
2. In the [index.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/execute-study/index.js), update the path on line 22 of the same file to match your directory's path. For directory `1`, it would be something like `/<your workspace>/1/sylius-showcase/restart.sh`.
3. In [cypress.config.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-test-suite/original/cypress.config.js) for each directory, update line 6 to point to the correct link. For instance, use "http://localhost:9991" for directory `1`.

#### Step 5: Running the Experiment

Go into each `execute-study` directory in the directories `0, 1, 2, 3, 4`, and execute the following command:

```bash
node index.js | tee ./without-delay-50-executions.txt
```

Tip: Replace without-delay with a name relevant to the directory, such as 500ms for directory 1, 1000ms for directory 2, and so on.

Another Tip: It's a good practice to use multiple Linux terminals to execute these commands concurrently.

This command will run the script index.js. This script execute the test suite 50 times on the respective Sylius instance and save the results in a .txt file for later analysis.

## Replication Material for Evaluating Different Waiting Mechanisms

### Setting Up Your Environment

**The steps 1, 2 and 3 are the same of the previous study. You can skip.**

#### Step 1: Directory and Repository Setup

1. Create five directories on your computer, and name them `0, 1, 2, 3, 4`.
2. Clone the following repositories into each directory:
    - [execute-study](https://gitlab.com/lsi-ufcg/cytestion/sync-study/execute-study)
    - [sylius-showcase](https://gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-showcase)
    - [sylius-test-suite](https://gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-test-suite)

For each directory:

- **Directory 0**: Use the `main` branch in the `sylius-showcase` repository.
- **Directory 1**: Use the `500ms` branch in the `sylius-showcase` repository.
- **Directory 2**: Use the `1000ms` branch in the `sylius-showcase` repository.
- **Directory 3**: Use the `1500ms` branch in the `sylius-showcase` repository.
- **Directory 4**: Use the `2000ms` branch in the `sylius-showcase` repository.

Make sure you've successfully checked out the correct branches and prepared the instances for the next steps.

#### Step 2: Sylius Showcase Configuration

Inside the `sylius-showcase` repository, you'll find instructions on building, running, and inserting sample data into the respective instance. Please follow these steps. 

#### Step 3: Setting Up Permissions and Script Paths

1. You will need to grant your user account permission to execute scripts with `sudo` without requiring a password. Follow this [guide](https://www.cyberciti.biz/faq/linux-unix-running-sudo-command-without-a-password/) to set this up.
2. We will use this [restart](https://gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-showcase/restart.sh)  script with `sudo` to reset the database of each container automatically. Ensure you adjust the path contained in `restart.sh` to match your current working directory. For example, replace it with `/<your workspace>/0/sylius-showcase/media`.

#### Step 4: Configuration for Delays Directories

We will set up the `execute-study` and `sylius-test-suite` repositories to work with Sylius instances running on ports different from the default (9990).
  
1. In the [index.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/execute-study/index.js) file for each directory, modify line 4 to point to the correct path. For example, for directory `1`, it should be `/<your workspace>/1/sylius-test-suite`, and so on.
2. In the [index.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/execute-study/index.js) file for each directory, modify line 5 to point to the correct test suite directory (static-wait, explicit-wait, wait-network, stable-dom). For example, for the static-wait execution, it should be `static-wait`.
3.  In the [index.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/execute-study/index.js), update the path on line 22 of the same file to match your directory's path. For directory `1`, it would be something like `/<your workspace>/1/sylius-showcase/restart.sh`.
4. In [cypress.config.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-test-suite/static-wait/cypress.config.js), for the static-wait execution, in static-wait directory, update line 6 to point to the correct link. Use "http://localhost:9991" for directory `1`. For the execution of the others mechanisms you should get the respective `cypress.config.js`. For example, [cypress.config.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-test-suite/explicit-wait/cypress.config.js) for explicit-wait, [cypress.config.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-test-suite/explicit-wait/cypress.config.js) for wait-network and [cypress.config.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-test-suite/explicit-wait/cypress.config.js) for stable-dom.

#### Step 5: Running the Experiment

Go into each `execute-study` directory in the directories `0, 1, 2, 3, 4`, and execute the following command:

```bash
node index.js | tee ./without-delay-50-executions.txt
```

Another Tip: It's a good practice to use multiple Linux terminals to execute these commands concurrently.

This command will run the script index.js. This script execute the test suite 50 times on the respective Sylius instance and save the results in a .txt file for later analysis.

**Execute one mechanism per time (static-wait, explicit-wait, wait-network, stable-dom). Remember to copy the .txt before execute another mechanism. The example provide will execute the static-wait mechanism. For the others mechanisms you should change only the step 4.2 and 4.5 and re-execute** 


## Replication Material for the Case Study with an Industrial Application

For the case study, the industrial application cannot be provided as it is confidential. However, this was the script used to execute each mechanism 50 times.

```js
const { execSync } = require('child_process');

for (let index = 0; index < 50; index++) {
  console.log('### STARTING EXECUTION: ' + (index + 1));
  try {
    execSync(
      'docker run --ipc=host --rm -v /workspace/sync-study/static-wait/<CONFIDENTIAL REPOSITORY>:/app -w /app <CONFIDENTIAL IMAGE> /bin/bash -c "/init.sh && cd tests && ./exec.sh"',
      { stdio: 'inherit' }
    );
  } catch (error) {}

  console.log('### ENDING EXECUTION: ' + (index + 1));
}
```

The script was executed with this command:

```bash
node index.js | tee ./static-wait-50-executions.txt
```

For the static-wait mechanism.

The results obtained can be found here: https://gitlab.com/lsi-ufcg/cytestion/sync-study/execute-study/case-study

 ## Data Analysis Scripts

To analyze the results, we've created several Node.js scripts to extract, catalog, and compute the obtained data.

#### [analysis.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/execute-study/analysis.js)

This script extracts the total number of passes, failures, pending, and skipped test cases, as well as the total execution time and average execution time. It collects this data from the total results table provided at the end of each Cypress suite execution. Be sure to adjust the path on line 5 when necessary.

#### [analysis-case-study.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/execute-study/analysis-case-study.js)

Similar to the previous script, this one performs the same process but focuses on the results obtained in the case study.

#### [analysis-hard.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/execute-study/analysis-hard.js)

This script conducts an analysis of passes and failures by inspecting the execution of individual test cases. It can extract failure messages for each script, as well as the quantity of failures per script and per test case.

#### [analysis-hard-sync-problems.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/execute-study/analysis-hard-sync-problems.js)

Similar to the previous script, this script conducts an analysis of passes and failures, but only for the 10 test cases identified with synchronization issues.

#### [analysis-count-failures-by-test-case.js](https://gitlab.com/lsi-ufcg/cytestion/sync-study/execute-study/analysis-count-failures-by-test-case.js)

This script extends the analysis performed by `analysis-hard.js` by focusing on counting the distinct number of failure messages that occurred in each instance across various executions.

These scripts are essential for extracting meaningful insights and statistics from the collected data. You can use them to evaluate the performance and behavior of the waiting mechanisms in detail.

Feel free to explore and modify these scripts as needed to further analyze the data according to your specific research objectives.
