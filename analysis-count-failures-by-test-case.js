const fs = require('fs');

const formatData = (object) => {
  const result = [];
  Object.keys(object).forEach((spec) => {
    Object.keys(object[spec]).forEach((test) => {
      const obj = {
        'test case': test,
        'quantity of failures': object[spec][test],
      };
      result.push(obj);
    });
  });
  return result
    .sort((a, b) => a['quantity of failures'] - b['quantity of failures'])
    .reverse();
};

const totalFile =
  'empirical-study/first-execution/results/all-count-failures-by-test-case.json';
let total = fs.readFileSync(totalFile);
total = JSON.parse(total);
total = formatData(total);

const dic = {};
let result = {};

let count = 1;
total.forEach((elem) => {
  let generic = '';
  if (dic[elem['test case']]) {
    generic = dic[elem['test case']];
  } else {
    generic = `Test Case ${count++}`;
    dic[elem['test case']] = generic;
  }
  result[generic] = elem['quantity of failures'];
});

console.log(dic);

const files = [
  'empirical-study/first-execution/results/without-count-failures-by-test-case.json',
  'empirical-study/first-execution/results/500ms-count-failures-by-test-case.json',
  'empirical-study/first-execution/results/1000ms-count-failures-by-test-case.json',
  'empirical-study/first-execution/results/1500ms-count-failures-by-test-case.json',
  'empirical-study/first-execution/results/2000ms-count-failures-by-test-case.json',
];

files.forEach((file) => {
  result = {};
  let testCaseFailing = fs.readFileSync(file);
  testCaseFailing = JSON.parse(testCaseFailing);
  testCaseFailing = formatData(testCaseFailing);

  Object.keys(dic).forEach((elem) => {
    result[dic[elem]] = 0;

    const fail = testCaseFailing.find((test) => test['test case'] === elem);
    if (fail) {
      let generic = dic[elem];
      result[generic] = fail['quantity of failures'];
    }
  });

  console.log(result);
});
