const { execSync } = require('child_process');
const spawn = require('cross-spawn');

const path = '/home/lsi/mestrado/sync-study/0/sylius-test-suite';
const approach = 'original'

let status = 0;
const options = ['test-suite'];
const execute = () => {
    const execution = spawn.sync('yarn', options, {
        stdio: 'inherit',
        cwd: `${path}/${approach}`,
    });
    if (execution.status !== 0) status = execution.status;
};

execSync(`cd ${path}/${approach} && yarn install`);

for (let index = 0; index < 50; index++) {
    console.log("### STARTING EXECUTION: " + (index + 1));
    execute();
    execSync('sudo -- sh -c \' bash /home/lsi/mestrado/sync-study/0/sylius-showcase/restart.sh\'');
    console.log("### ENDING EXECUTION: " + (index + 1));
}