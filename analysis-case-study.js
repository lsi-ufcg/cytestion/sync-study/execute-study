const fs = require('fs');
const csv = require('csvtojson');
let result = fs
  .readFileSync('case-study/1.static-wait-25-executions.txt')
  .toString();
result = result.split('### ENDING EXECUTION');

function parseTable(input) {
  const lines = input.trim().split('\n');
  const dataRows = [];
  let summaryRow = null;
  let allSpecsPassedRow = null;

  for (const line of lines) {
    const matchData = line.match(
      /([✔✖])\s+([\w.-]+)\s+([\d:]+)\s+(\d+)\s+(\d+|-)\s+(\d+|-)\s+(\d+|-)\s+(\d+|-)/
    );
    const matchSummary = line.match(
      /([✔✖])\s+(\d+)\s+of\s+(\d+)\s+(failed|passed)\s+\((\d+%)\)\s+([\d:]+)\s+(\d+)\s+(\d+)\s+(\d+|-)\s+(\d+|-)\s+(\d+|-)/
    );
    const matchAllSpecsPassed = line.match(
      /✔\s+All specs passed!\s+([\d:]+)\s+(\d+)\s+(\d+)\s+(\d+|-)\s+(\d+|-)\s+(\d+|-)/
    );

    if (matchData) {
      dataRows.push({
        status: matchData[1],
        spec: matchData[2],
        time: matchData[3],
        tests: matchData[4],
        passing: matchData[5],
        failing: matchData[6],
        pending: matchData[7],
        skipped: matchData[8],
      });
    } else if (matchSummary) {
      summaryRow = {
        status: matchSummary[1],
        failed: matchSummary[2],
        total: matchSummary[3],
        failurePercentage: matchSummary[5],
        time: matchSummary[6],
        tests: matchSummary[7],
        passing: matchSummary[8],
        failing: matchSummary[9],
        pending: matchSummary[10],
        skipped: matchSummary[11],
      };
    } else if (matchAllSpecsPassed) {
      allSpecsPassedRow = {
        status: '✔',
        spec: 'All specs passed!',
        time: matchAllSpecsPassed[1],
        tests: matchAllSpecsPassed[2],
        passing: matchAllSpecsPassed[3],
        failing: matchAllSpecsPassed[4],
        pending: matchAllSpecsPassed[5],
        skipped: matchAllSpecsPassed[6],
      };
    }
  }
  return { dataRows, summaryRow, allSpecsPassedRow };
}

function generateCSV(data) {
  const { dataRows, summaryRow, allSpecsPassedRow } = data;
  let csv = 'Spec,Time,Tests,Passing,Failing,Pending,Skipped\n';

  for (const row of dataRows) {
    csv += `${row.spec},${row.time},${row.tests},${row.passing},${row.failing},${row.pending},${row.skipped}\n`;
  }

  if (allSpecsPassedRow) {
    csv += `${allSpecsPassedRow.spec},${allSpecsPassedRow.time},${allSpecsPassedRow.tests},${allSpecsPassedRow.passing},${allSpecsPassedRow.failing},${allSpecsPassedRow.pending},${allSpecsPassedRow.skipped}\n`;
  }

  if (summaryRow) {
    csv += `${summaryRow.failed} of ${summaryRow.total} failed (${summaryRow.failurePercentage}),${summaryRow.time},${summaryRow.tests},${summaryRow.passing},${summaryRow.failing},${summaryRow.pending},${summaryRow.skipped}\n`;
  }

  return csv;
}

function convertTimeToSeconds(time) {
  const count = (time.match(/:/g) || []).length;
  if (count === 1) {
    const [minutes, seconds] = time.split(':').map(Number);
    return minutes * 60 + seconds;
  } else if (count === 2) {
    const [hours, minutes, seconds] = time.split(':').map(Number);
    return hours * 3600 + minutes * 60 + seconds;
  } else {
    const [seconds] = [time].map(Number);
    return seconds;
  }
}

function analyzeCombinedCSVs(combinedCSVs) {
  const countBySpec = {};

  const analysis = {
    totalPassing: 0,
    totalFailing: 0,
    totalPending: 0,
    totalSkipped: 0,
    totalTimeInSeconds: 0,
    totalExecutions: combinedCSVs.length,
    averageTimeInSeconds: 0,
  };

  const calculate = (analysis, countBySpec, combinedCSVs) => {
    if (combinedCSVs.length === 0) {
      analysis.averageTimeInSeconds =
        analysis.totalTimeInSeconds / analysis.totalExecutions;
      const result = {};
      Object.keys(countBySpec).forEach((spec) =>
        countBySpec[spec] ? (result[spec] = countBySpec[spec]) : null
      );
      console.log(result);
      console.log(analysis);
    } else {
      const csvData = combinedCSVs.shift();
      let dataRows = csvData.split('\n');
      dataRows.shift();
      csv()
        .fromString(dataRows.join('\n'))
        .then(function (jsonArrayObj) {
          jsonArrayObj.forEach((obj, idx) => {
            if (idx === jsonArrayObj.length - 1) {
              analysis['totalPassing'] += parseInt(obj['Passing'], 10) || 0;
              analysis['totalFailing'] += parseInt(obj['Failing'], 10) || 0;
              analysis['totalPending'] += parseInt(obj['Pending'], 10) || 0;
              analysis['totalSkipped'] += parseInt(obj['Skipped'], 10) || 0;
              analysis['totalTimeInSeconds'] += convertTimeToSeconds(
                obj['Time']
              );
            } else {
              const spec = obj['Spec'].replace('.cy.js', '');
              if (!countBySpec[spec]) countBySpec[spec] = 0;
              countBySpec[spec] += parseInt(obj['Failing'], 10) || 0;
            }
          });
          return calculate(analysis, countBySpec, combinedCSVs);
        });
    }
  };

  calculate(analysis, countBySpec, combinedCSVs);
}

function processExecutionResults(executions) {
  const combinedCSVs = [];

  executions.forEach((execution, index) => {
    const parsedData = parseTable(execution);

    let csv = generateCSV(parsedData);
    // Add the execution number at the beginning of each CSV content
    csv = `Execution,${index + 1}\n${csv}`;

    combinedCSVs.push(csv);
  });
  combinedCSVs.pop();

  // Combining all CSVs into one
  const finalCSV = combinedCSVs.join('\n\n');

  analyzeCombinedCSVs(combinedCSVs);

  return finalCSV;
}

const executions = result.map((table) => {
  const start = table.lastIndexOf('Spec') - 5;
  return table.slice(start).replace('      Tests', 'Time  Tests');
});
const combinedCSV = processExecutionResults(executions);

// Write the combined CSV to a file
fs.writeFileSync('combined_results.csv', combinedCSV);
