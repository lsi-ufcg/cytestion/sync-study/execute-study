const fs = require('fs');
// read dir and iterate
// const files = [
//   'original/500ms-delay-50-executions.txt',
//   'original/1000ms-delay-50-executions.txt',
//   'original/1500ms-delay-50-executions.txt',
//   'original/2000ms-delay-50-executions.txt',
//   'original/without-delay-50-executions.txt',
// ];

// let executions = [];
// files.forEach((file) => {
//   let fileExecutions = fs.readFileSync(file).toString();
//   fileExecutions = fileExecutions.split('### ENDING EXECUTION');
//   fileExecutions.pop();
//   executions.push(...fileExecutions);
// });

let first = true;
let suiteFail = 0;
let suitePassed = 0;

let executions = fs
  .readFileSync(
    'empirical-study/first-execution/original/2000ms-delay-50-executions.txt'
  )
  .toString();
executions = executions.split('### ENDING EXECUTION');
executions.pop();

let testCasesBySpec = fs.readFileSync('data.json');
testCasesBySpec = JSON.parse(testCasesBySpec);

let result = {};
let countBySpec = {};
let countByTestCase = {};
let countTotalFail = 0;
let countTotalSkipped = 0;
const finalEnd = '(Results)\n';
const testCasesDuplicated = [
  'validate that cannot create the same currency twice',
  'verifica se o filtro "contains" com a palavra "AUD" encontra o elemento',
  'verifica se o filtro "not contains" com a palvra "AUD" não mostra o campo correspondente',
  'verifica se salvar as informações na parte de edição cria uma mensagem de sucesso',
  'should not create new offline payment method with invalid code',
  'test filter button',
];

const testCaseWithSyncIssue = [
  'Add a association product',
  'defines a type of association between one product and another',
  'delete a type of association between one product and another',
  'should see the details of a product size "S" and add to cart',
  'should see the details of a product size "XXL" and add to cart',
  'should see the details of a product size "M" and add to cart',
  'should see the details of a product size "L" and add to cart',
  'should see the details of a product size "XL" and add to cart',
  'filter on a especific product',
  'create new taxon',
];

executions.forEach((execution) => {
  if (first) suitePassed++;
  first = true;
  for (const [specName, testCases] of Object.entries(testCasesBySpec)) {
    const testCasesFiltered = [...new Set(testCases)];
    let cut;
    let specFormatted = 'Running:  ' + specName.replace(' ', '-') + '.cy.js';

    cut = execution.slice(execution.indexOf(`${specFormatted}`));
    cut = cut
      .substring(0, cut.indexOf(finalEnd) + finalEnd.length)
      .replace(/^.*      at .*$/gm, '')
      .replace(/(\r\n|\r|\n){2}/g, '$1')
      .replace(/(\r\n|\r|\n){3,}/g, '$1\n');

    for (let index = 0; index < testCasesFiltered.length; index++) {
      const testCase = testCasesFiltered[index];
      let fails = 0;

      if (testCasesDuplicated.includes(testCase)) {
        fails = 2;
        const lines = cut.split('\n');
        const linesWithTiming = [];
        for (const line of lines) {
          if (/\(\d+ms\)$/.test(line)) {
            linesWithTiming.push(line);
          }
        }
        linesWithTiming.forEach((line) => {
          if (line.includes(testCase)) fails--;
        });
      }

      if (!cut.includes(`✓ ${testCase}`) || fails > 0) {
        if (!fails) fails = 1;
        countTotalFail += fails;

        let error;
        let start = `${testCase}:\n`;
        if (!cut.includes(start)) {
          start = `"${testCase}":\n`;
        }
        if (!cut.includes(start)) {
          error = `Test skipped`;
          countTotalFail -= fails;
          countTotalSkipped += fails;
        }
        let end = finalEnd;
        if (testCasesFiltered[index + 1]) {
          end = `${testCasesFiltered[index + 1]}:\n`;
        }
        if (!error) {
          error = cut.slice(cut.indexOf(start), cut.indexOf(end));
          const pattern = new RegExp(`${testCase}:\n`, 'g');
          error = error.replace(pattern, '');
          if (testCasesFiltered[index + 1]) {
            error = error.split('\n');
            error.pop();
            error.pop();
            error = error.join('\n');
          }
          if (
            testCaseWithSyncIssue.includes(testCase) &&
            !error.includes(
              'an only be called on a single element. Your subject contained 2 elements.'
            )
          ) {
            if (first) {
              first = false;
              suiteFail++;
            }
            if (!result[specName]) {
              result[specName] = { [testCase]: new Set() };
              countByTestCase[specName] = { [testCase]: 0 };
              countBySpec[specName] = 0;
            }
            if (!result[specName][testCase]) {
              result[specName][testCase] = new Set();
              countByTestCase[specName][testCase] = 0;
            }
            result[specName][testCase].add(error);
            countByTestCase[specName][testCase] += fails;
            countBySpec[specName] += fails;
          }
        }
      }
    }
  }
});

Object.keys(result).forEach((specName) => {
  Object.keys(result[specName]).forEach((testCase) => {
    result[specName][testCase] = [...result[specName][testCase]];
  });
});

console.log(result);
console.log(countByTestCase);
console.log(countBySpec);
fs.writeFileSync('result.json', JSON.stringify(result));
fs.writeFileSync('countByTestCase.json', JSON.stringify(countByTestCase));
fs.writeFileSync('countBySpec.json', JSON.stringify(countBySpec));

console.log('Count total failures: ' + countTotalFail);
console.log('Count total skipped: ' + countTotalSkipped);
console.log('Count total: ' + (countTotalSkipped + countTotalFail));

let resultado = 0;
Object.keys(countBySpec).forEach((elem) => (resultado += countBySpec[elem]));

console.log('failures with sync issue: ' + resultado);
console.log('suite fail: ' + suiteFail);
console.log('suite passed: ' + suitePassed);
